function primeState(){

    request = new XMLHttpRequest()

    request.open("GET", "https://cors-anywhere.herokuapp.com/https://www.coincalculators.io/api?name=scprime");
    request.onload = function() {

        // Begin accessing JSON data here

        var data = JSON.parse(this.response)

        if (request.status >= 200 && request.status < 400) {
            document.getElementById('name').innerHTML = (data.name)
            document.getElementById('hash-rate').innerHTML =  ((data.currentNethash)/1000000000000).toFixed(2);
            document.getElementById('price-btc').innerHTML = (data.price_btc).toFixed(8);
            document.getElementById('price-usd').innerHTML = (data.price_usd).toFixed(5);
            document.getElementById('volume_usd').innerHTML = (data.volume_usd).toFixed(2);
        }else{
            console.log('error')
        } 
        let profitability = function(){

            moment().format(); // from https://github.com/KizmoTek/Mining-Calculator/blob/master/Blake2b.js
            const SCPDevFeeStart = moment('04/30/2019').unix();
            const SCPDevFeeEnd = moment('10/31/2020').unix();
            var today = new Date();
            var dd = String(today.getDate()).padStart(2, '0');
            var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
            var yyyy = today.getFullYear();
            var newToday = yyyy + '-' + mm + '-' + dd

            let currDate = moment(newToday).unix()

            let SCPDevFee = .20  // Starts at 20%
            if (currDate > SCPDevFeeStart) {
                if (currDate < SCPDevFeeEnd) {
                    SCPDevFee = (0.2 - (SCPDevFeeEnd - currDate) * (0.1 / (SCPDevFeeEnd - SCPDevFeeStart)))
                } else {
                    SCPDevFee = 0
                }
            }

            //Get the user input data

            let hashRate = document.getElementById("hashRate").value;

            //Prosess user's input data

            let hashrateGhash = hashRate * 1000000000000;
            let difficulty = data.currentDifficulty;
            let primeBlockTime = data.blockTime;
            let blockHeight = data.lastBlock;
            let period = 3600;
            
            let hourlyResult = (hashrateGhash / ((difficulty / primeBlockTime) + hashrateGhash)) * ((300 - (blockHeight / 1000) - ((period / primeBlockTime) / 2000)) * (period / primeBlockTime) * (1 - SCPDevFee));
            let dailyResult = hourlyResult * 24;
            let weeklyResult = dailyResult * 7;
            let monthlyResult = weeklyResult * 4;
            let yearlyResult = monthlyResult * 12;

            document.getElementById("hourlyResult").innerHTML = hourlyResult.toFixed(2);
            document.getElementById("dailyResult").innerHTML = dailyResult.toFixed(2);
            document.getElementById("weeklyResult").innerHTML = weeklyResult.toFixed(2);
            document.getElementById("monthlyResult").innerHTML = monthlyResult.toFixed(2);
            document.getElementById("yearlyResult").innerHTML = yearlyResult.toFixed(2);

            // Revenue in BTC

            let hourlyRevBtc = hourlyResult * (data.price_btc);
            let dailyRevBtc = hourlyRevBtc * 24;
            let weeklyRevBtc = dailyRevBtc * 7;
            let monthlyRevBtc = weeklyRevBtc * 4;
            let yearlyRevBtc = monthlyRevBtc * 12;
            document.getElementById('hourlyRevBtc').innerHTML = hourlyRevBtc.toFixed(8);
            document.getElementById('dailyRevBtc').innerHTML = dailyRevBtc.toFixed(8);
            document.getElementById('weeklyRevBtc').innerHTML = weeklyRevBtc.toFixed(8);
            document.getElementById('monthlyRevBtc').innerHTML = monthlyRevBtc.toFixed(8);
            document.getElementById('yearlyRevBtc').innerHTML = yearlyRevBtc.toFixed(8);


            // Revenue In Usd

            let hourlyRevUsd = hourlyResult * (data.price_usd);
            let dailyRevUsd = hourlyRevUsd * 24;
            let weeklyRevUsd = dailyRevUsd * 7;
            let monthlyRevUsd = weeklyRevUsd * 4;
            let yearlyRevUsd = monthlyRevUsd * 12;
            document.getElementById('hourlyRevUsd').innerHTML = hourlyRevUsd.toFixed(2);
            document.getElementById('dailyRevUsd').innerHTML = dailyRevUsd.toFixed(2);
            document.getElementById('weeklyRevUsd').innerHTML = weeklyRevUsd.toFixed(2);
            document.getElementById('monthlyRevUsd').innerHTML = monthlyRevUsd.toFixed(2);
            document.getElementById('yearlyRevUsd').innerHTML = yearlyRevUsd.toFixed(2);

            //Cost

            let cost = document.getElementById("cost").value;
            let power = document.getElementById("power").value / 1000; // convert W -> kW
            let totalCost = cost * power;
            let hourlyCost = document.getElementById("hourlyCost").innerHTML = totalCost.toFixed(2);
            let dailyCost = document.getElementById("dailyCost").innerHTML = (totalCost * 24).toFixed(2);
            let weeklyCost = document.getElementById("weeklyCost").innerHTML = (totalCost * 24 * 7).toFixed(2);
            let monthlyCost = document.getElementById("monthlyCost").innerHTML =( totalCost * 24 * 7 * 4).toFixed(2);
            let yearlyCost = document.getElementById("yearlyCost").innerHTML = (totalCost *24 * 7 * 4 * 12).toFixed(2);

            // Net Profit USD

            let hourlyNetProfit = hourlyRevUsd - hourlyCost;
            let dailyNetProfit = dailyRevUsd - dailyCost;
            let weeklyNetProfit = weeklyRevUsd - weeklyCost;
            let monthlyNetProfit = monthlyRevUsd - monthlyCost;
            let yearlyNetProfit = yearlyRevUsd - yearlyCost;

            document.getElementById("hourlyNetProfit").innerHTML = hourlyNetProfit.toFixed(2);
            document.getElementById("dailyNetProfit").innerHTML = dailyNetProfit.toFixed(2);
            document.getElementById("weeklyNetProfit").innerHTML = weeklyNetProfit.toFixed(2);
            document.getElementById("monthlyNetProfit").innerHTML = monthlyNetProfit.toFixed(2);
            document.getElementById("yearlyNetProfit").innerHTML = yearlyNetProfit.toFixed(2);

            // The Electricity cost to breakeven

            document.getElementById("electricityCost").innerHTML = hourlyRevUsd / power;
        
        }
        document.getElementById("calculate").addEventListener('click', profitability )
    }
    request.send()
} 
primeState()


// // southxchange
// function primeStateSouthXchange(){

//     request = new XMLHttpRequest()

//     request.open("GET", "https://cors-anywhere.herokuapp.com/https://www.southxchange.com/api/price/SCP/USD");
//     request.onload = function() {

//         // Begin accessing JSON data here

//         var data = JSON.parse(this.response)

//         if (request.status >= 200 && request.status < 400) {
//             document.getElementById('Volume-USD-SouthXchange').innerHTML = (data.Volume24Hr)
//         }else{
//             console.log('error')
//         } 
//     }
//     request.send()
// }
// primeStateSouthXchange()
